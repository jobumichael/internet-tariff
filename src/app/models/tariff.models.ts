export interface ITariffData {
  id: number;
  tariffName: string;
  price: number;
  currency: string;
  speed: {
    download: ISpeedData;
    upload: ISpeedData;
  };
  highlights: string[];
}

interface ISpeedData {
  value: number;
  unit: string;
}
