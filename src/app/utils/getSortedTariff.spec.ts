import { getSortedTariff } from './getSortedTariff';

import { mockTariffData } from '../mock/mock-tariff-data';

describe('getSortedTariff', () => {
  it('should sort data by price in ascending order', () => {
    const sortedData = getSortedTariff('priceAsc', mockTariffData);

    expect(sortedData[0].tariffName).toBe('O2');
    expect(sortedData[1].tariffName).toBe('1&1');
    expect(sortedData[2].tariffName).toBe('Vodafone Cable Germany');
    expect(sortedData[3].tariffName).toBe('Telekom');
  });

  it('should sort data by price in descending order', () => {
    const sortedData = getSortedTariff('priceDesc', mockTariffData);

    expect(sortedData[0].tariffName).toBe('Telekom');
    expect(sortedData[1].tariffName).toBe('Vodafone Cable Germany');
    expect(sortedData[2].tariffName).toBe('1&1');
    expect(sortedData[3].tariffName).toBe('O2');
  });

  it('should return unsorted data for an invalid sorting option', () => {
    const sortedData = getSortedTariff('invalidOption', mockTariffData);

    expect(sortedData).toEqual(mockTariffData);
  });
});
