import { ITariffData } from '../models/tariff.models';

export const getSortedTariff = (sortingOption: string, data: ITariffData[]) => {
  switch (sortingOption) {
    case 'priceAsc':
      return data.sort((a, b) => a.price - b.price);

    case 'priceDesc':
      return data.sort((a, b) => b.price - a.price);

    case 'downloadSpeedAsc':
      return data.sort(
        (a, b) => a.speed.download.value - b.speed.download.value
      );

    case 'downloadSpeedDesc':
      return data.sort(
        (a, b) => b.speed.download.value - a.speed.download.value
      );

    case 'uploadSpeedAsc':
      return data.sort((a, b) => a.speed.upload.value - b.speed.upload.value);
    case 'uploadSpeedDesc':
      return data.sort((a, b) => b.speed.upload.value - a.speed.upload.value);

    default:
      return data;
  }
};
