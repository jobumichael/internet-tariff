export const mockTariffData = [
  {
    id: 1,
    tariffName: 'Vodafone Cable Germany',
    price: 39.99,
    currency: '€',
    speed: {
      download: {
        value: 200,
        unit: 'Mbit/s',
      },
      upload: {
        value: 10,
        unit: 'Mbit/s',
      },
    },
    highlights: ['Internet Flatrate', 'Telefon Flatrate', 'Fernsehen Flatrate'],
  },
  {
    id: 2,
    tariffName: '1&1',
    price: 15.99,
    currency: '€',
    speed: {
      download: {
        value: 100,
        unit: 'Mbit/s',
      },
      upload: {
        value: 40,
        unit: 'Mbit/s',
      },
    },
    highlights: ['Internet Flatrate', 'Telefon Flatrate', 'Fernsehen Flatrate'],
  },

  {
    id: 3,
    tariffName: 'O2',
    price: 13.99,
    currency: '€',
    speed: {
      download: {
        value: 50,
        unit: 'Mbit/s',
      },
      upload: {
        value: 5,
        unit: 'Mbit/s',
      },
    },
    highlights: ['Internet Flatrate', 'Telefon Flatrate', 'Fernsehen Flatrate'],
  },
  {
    id: 4,
    tariffName: 'Telekom',
    price: 59.99,
    currency: '€',
    speed: {
      download: {
        value: 800,
        unit: 'Mbit/s',
      },
      upload: {
        value: 30,
        unit: 'Mbit/s',
      },
    },
    highlights: ['Internet Flatrate', 'Telefon Flatrate', 'Fernsehen Flatrate'],
  },
];
