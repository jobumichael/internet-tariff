import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent],
    });

    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should set the default label', () => {
    expect(component.label).toBe('Button');
  });

  it('should emit a click event when clicked', () => {
    let clicked = false;
    component.clicked.subscribe(() => {
      clicked = true;
    });

    component.onClick();
    expect(clicked).toBe(true);
  });

  it('should render the label in the button', () => {
    const buttonElement: HTMLElement =
      fixture.nativeElement.querySelector('button');
    expect(buttonElement.textContent).toContain('Button');
  });
});
