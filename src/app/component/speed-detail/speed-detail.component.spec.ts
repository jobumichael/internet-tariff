import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppSpeedDetailComponent } from './speed-detail.component';

describe('SpeedDetailComponent', () => {
  let component: AppSpeedDetailComponent;
  let fixture: ComponentFixture<AppSpeedDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppSpeedDetailComponent],
    });

    fixture = TestBed.createComponent(AppSpeedDetailComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should set the default background color', () => {
    fixture.detectChanges();
    const element: HTMLElement = fixture.nativeElement;
    const speedDetailElement = element.querySelector(
      '.speed-box'
    ) as HTMLElement;

    expect(speedDetailElement.style.backgroundColor).toBe('rgb(255, 255, 255)');
  });

  it('should display the speed and unit in the template', () => {
    component.speed = 50;
    component.unit = 'Mbit/s';
    fixture.detectChanges();

    const element: HTMLElement = fixture.nativeElement;
    const speedElement = element.querySelector('.speed-value') as HTMLElement;
    const unitElement = element.querySelector('.speed-info') as HTMLElement;

    expect(speedElement.textContent).toContain('50');
    expect(unitElement.textContent).toContain('Mbit/s');
  });

  it('should display the title in the template', () => {
    component.title = 'Upload';
    fixture.detectChanges();

    const element: HTMLElement = fixture.nativeElement;
    const titleElement = element.querySelector('.title') as HTMLElement;

    expect(titleElement.textContent).toContain('Upload');
  });

  it('should apply the icon class if provided', () => {
    component.iconClass = 'fa fa-cloud';
    fixture.detectChanges();

    const element: HTMLElement = fixture.nativeElement;
    const iconElement = element.querySelector('.speed-icon i') as HTMLElement;

    expect(iconElement).toBeTruthy();
    expect(iconElement.classList.contains('fa')).toBeTruthy();
    expect(iconElement.classList.contains('fa-cloud')).toBeTruthy();
  });
});
