import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-speed-detail',
  templateUrl: './speed-detail.component.html',
  styleUrls: ['./speed-detail.component.sass'],
})
export class AppSpeedDetailComponent {
  @Input() backgroundColor: string = '#fff';
  @Input() speed: number = 0;
  @Input() unit: string = 'Mbit/s';
  @Input() title: string = 'Download';
  @Input() iconClass?: string;

  constructor() {}
}
