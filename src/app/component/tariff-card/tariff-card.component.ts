import { Component, Input } from '@angular/core';
import { ITariffData } from 'src/app/models/tariff.models';

@Component({
  selector: 'app-tariff-card',
  templateUrl: './tariff-card.component.html',
  styleUrls: ['./tariff-card.component.sass'],
})
export class TariffCardComponent {
  constructor() {}

  @Input() tariff: ITariffData = {} as ITariffData;
  @Input() id: number = 0;

  handleButtonClick(): void {
    console.log('Button clicked!');
  }
}
