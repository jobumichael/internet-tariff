import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { ButtonComponent } from './component/button/button.component';
import { AppHeaderComponent } from './component/header/header.component';
import { AppSpeedDetailComponent } from './component/speed-detail/speed-detail.component';
import { TariffCardComponent } from './component/tariff-card/tariff-card.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    TariffCardComponent,
    AppSpeedDetailComponent,
    ButtonComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
