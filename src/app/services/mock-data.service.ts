// mock-data.service.ts

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { mockTariffData } from '../mock/mock-tariff-data';

@Injectable({
  providedIn: 'root',
})
export class MockDataService {
  constructor(private http: HttpClient) {}

  getMockData(): Observable<any> {
    return of(mockTariffData);
  }
}
