import { Component } from '@angular/core';
import { ITariffData } from './models/tariff.models';
import { MockDataService } from './services/mock-data.service';
import { getSortedTariff } from './utils/getSortedTariff';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  title = 'internet-tariff';
  selectedSortOption: string = 'priceAsc';
  sortedProducts: ITariffData[] = [];
  tariffData: ITariffData[] = [];

  constructor(private mockDataService: MockDataService) {}

  sortProducts() {
    this.sortedProducts = getSortedTariff(
      this.selectedSortOption,
      this.tariffData
    );
  }

  ngOnInit() {
    this.mockDataService.getMockData().subscribe((data) => {
      this.tariffData = data;
      this.sortedProducts = getSortedTariff(
        this.selectedSortOption,
        this.tariffData
      );
    });
  }
}
