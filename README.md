# InternetTariff

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.5.

Demo : https://internet-tariff-git-main-jobemichael.vercel.app/

## To run in local

```
npm install

npm run start
```

## To run test
```
npm run test
```

